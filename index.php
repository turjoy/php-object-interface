<?php

require 'TalkInterface.php';
require 'Dog.php';
require 'Cat.php';

$dog = new Dog;
$cat = new Cat;

echo $dog->talk() . PHP_EOL;
echo $cat->talk() . PHP_EOL;
