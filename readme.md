PHP-Object-Interface
PHP-Object-Interface
* interface একটি class এর blueprint.
* এটি simply একটি contract.
* একটি class এ কি কি method থাকবে সেটি strict করে দেয়া যায় interface এর মাধ্যমে। অর্থাৎ, Object Interface একটি class কে বাধ্য করে interface এর সবগুলো method কে implement করতে।
* interface এ method গুলোর কোন body থাকতে পারবেনা।
* যে class এর উপর কোন interface implement করা হবে সেই class যদি সাধারণ কোন class হয় তবে সেই class এ অবশ্যই interface এর সবগুলো method implement করতে হবে, মানে হল সেগুলোর body থাকতে হবে অবশ্যই।
* তবে সেটি যদি abstract class হয় তবে method গুলো declare করতে হবে, কিন্তু body না থাকলেও চলবে।