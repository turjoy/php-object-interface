<?php

interface TalkInterface {

    public function share();

    public function talk();

}
